package com.qaneh.core;

import org.junit.jupiter.api.Test;

import java.util.function.BiFunction;
import java.util.function.Function;

import static com.qaneh.core.Either.left;
import static com.qaneh.core.Either.right;
import static java.util.function.Function.identity;
import static org.assertj.core.api.Assertions.assertThat;

class LeftTest {

    private String leftValue = "Left";
    private Left<String, String> left = left(leftValue);
    private Function<Integer, Boolean> f = integer -> integer > 37;
    private Function<Object, Integer> g = Object::hashCode;

    @Test
    void testMap_WhenMappingWithId_SameAsCallingIdWithoutMap() {
        assertThat(left.map(identity()))
                .isEqualTo(identity().apply(left));
    }

    @Test
    void testMap_When_MappingWithAComposition_SameAsCallingMapOnIndividualFunctions() {
        var mapOverComposition = left.map(f.compose(g));
        var mapIndividually = left.map(g).map(f);
        assertThat(mapOverComposition).isEqualTo(mapIndividually);
    }

    @Test
    void testApply_When_CalledWithAnyFunction_ReturnLeftUnchanged() {
        assertThat(left.apply(right(null))).isEqualTo(left);
    }

    @Test
    void testApply_WhenMappingWithId_SameAsCallingIdWithoutMap() {
        assertThat(left.apply(right(identity()))).isEqualTo(left);
    }

    @Test
    void test_apply_WhenUsingAnyFunctionEither_ReturnsLeftUnchanged() {
        assertThat(left.apply(right(String::length))).isEqualTo(left);
    }

    @Test
    void test_apply_WhenUsingAnyBiFunction_ReturnsLeftUnchanged() {
        BiFunction<String, String, String> bf = (s, s2) -> s + s2;
        assertThat(left.apply(bf, left(""))).isEqualTo(left);
    }

    @Test
    void test_always_WhenUsingAnyArgument_ReturnsTheArgument() {
        var either = left("");
        assertThat(left.always(either)).isEqualTo(either);
    }

    @Test
    void test_flatMap_WhenUsingAnyFunction_ReturnsLeftUnchanged() {
        assertThat(left.flatMap(Either::right)).isEqualTo(left);
    }

}