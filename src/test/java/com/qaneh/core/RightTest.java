package com.qaneh.core;

import org.junit.jupiter.api.Test;

import java.util.Objects;
import java.util.function.Function;

import static com.qaneh.core.Either.right;
import static org.assertj.core.api.Assertions.assertThat;

class RightTest {
    private String rightValue = "Right";
    private Right<String, String> right = right(rightValue);
    private Function<Integer, Boolean> f = integer -> integer > 37;
    private Function<String, Integer> g = String::length;

    // Functor Identity
    @Test
    void testMap_When_MappingWithId_SameAsCallingIdWithoutMap() {
        assertThat(right.map(Function.identity()))
                .isEqualTo(Function.identity().apply(right));
    }

    // Functor composition
    @Test
    void testMap_When_MappingWithACompositionOfTwoFunctions_SameAsCallingMapOnIndividualFunctions() {
        Function<Integer, Boolean> f = integer -> integer > 37;
        var mapOverComposition = right.map(f.compose(g));
        var mapIndividually = right.map(g).map(f);
        assertThat(mapOverComposition).isEqualTo(mapIndividually);
    }

    @Test
    void testApply_When_CalledWithAnyRightFunction_FunctionIsApplied() {
        Either<String, Function<String, Boolean>> functionEither = new Right<>(Objects::isNull);
        assertThat(right.apply(functionEither));
    }

    // Applicative Identity
    @Test
    void testApply_When_MappingWithId_SameAsCallingIdWithoutMap() {
        assertThat(right.apply(right(Function.identity()))).isEqualTo(right);
    }

    // Applicative Composition
    @Test
    void testApply_When_MappingWithACompositionOfTwoFunctions_SameAsCallingMapOnIndividualFunctions() {
        Either<String, Function<String, Integer>> eitherG = right(g);
        Either<String, Function<Integer, Boolean>> eitherF = right(f);
        // TODO: finish test
    }

    @Test
    void test_always_WhenPassingAnyEither_ReturnsAnyEither() {
        Either<String, String> e = right("");
        assertThat(right.always(e)).isEqualTo(e);
    }

}