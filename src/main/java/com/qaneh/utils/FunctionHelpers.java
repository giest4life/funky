package com.qaneh.utils;

import java.util.function.BiFunction;
import java.util.function.Function;

public final class FunctionHelpers {
    /**
     * Returns a new {@link Function} that calls {@code biFunction} with the {@code t} as the first argument and passes the functions own argument as the second argument to {@code biFunction}
     *
     * @param biFunction the {@link BiFunction} that needs to be partially applied with {@code t}
     * @param t          the first argument to {@code biFunction}
     * @param <T>        the type of {@code t} and first argument to {@code biFunction}
     * @param <U>        the type of second argument to {@code biFunction}
     * @param <V>        the type of the return value for {@code biFunction}
     * @return a partially applied {@code biFunction}
     */
    public static <T, U, V> Function<U, V> partial(BiFunction<? super T, ? super U, ? extends V> biFunction, T t) {
        return u -> biFunction.apply(t, u);
    }
}
