package com.qaneh.core;

import java.util.function.BiFunction;
import java.util.function.Function;

/**
 * The Either type represents a result of a computation that may fail or succeed. Implementations of this interfaces are Functors, Applicative Functors, and Monads.
 *
 * @param <L> type of the value that represents failure
 * @param <R> the type of the value that represents success
 */
public interface Either<L, R> {

    /**
     * Implementations must ensure that mapper is only applied to a success value
     *
     * @param mapper a simple function that will map over the data in the Functor instance
     * @param <U>    the type of the success value after {@code mapper} is applied to it
     * @return an instance of Either with the mapper applied to the success value
     */
    <U> Either<L, U> map(Function<? super R, ? extends U> mapper);

    /**
     * Implementations must ensure that this method is only applied when the instance and {@code functionEither} are success
     *
     * @param functionEither and Either type that represents a function that may or may not be present
     * @param <U>            the return type of the function in functionEither
     * @return an new instance of Either after applying the function embedded in {@code functionEither} the current instance
     */

    <U> Either<L, U> apply(Either<L, ? extends Function<? super R, ? extends U>> functionEither);

    /**
     * Applies the {@code biMapper} to the values inside the current instance and secondEither. This is a convenience method and the same functionality can be achieved with {@link #apply(Either)}
     *
     * @param biMapper     the function that needs to be lifted over the current and {@code secondEither} instances of Either
     * @param secondEither the {@link Either} instance that potentially has the secondEither argument for {@code biMapper}
     * @param <U>          the type value that is potentially inside {@code secondEither}
     * @param <V>          the type of the return value of the {@code biMapper}
     * @return an instance of Either with the return value of {@code biMapper} after it is applied to the value of current instance and {@code secondEither}
     */
    <U, V> Either<L, V> apply(BiFunction<? super R, ? super U, ? extends V> biMapper, Either<L, U> secondEither);

    /**
     * Applies the {@code mapper} function, which itself returns an Either, and then flattens the result so that the Either is not nested in another Either.
     * Implementations should ensure that the {@code flatMap} obeys the 3 Monad laws
     *
     * @param mapper a function that takes any value of type {@code R} but returns a value of type {@code Either<L,U>}
     * @param <U>    the return type of matter that is embedded inside an Either
     * @return the result of applying {@code mapper} over the value inside of the instance
     */
    <U> Either<L, U> flatMap(Function<? super R, ? extends Either<L, U>> mapper);

    /**
     * The default implementation composes the current instance with {@code either} by simply returning it.
     * Any overriding implementations must ensure that, eventually, {@code either} is returned
     *
     * @param either the argument to be returned
     * @param <U>    the type of {@code either}
     * @return {@code either}
     */
    default <U> Either<L, U> always(Either<L, U> either) {
        return either;
    }

    /**
     * Allows clients to safely retrieve the value embedded in the instance. Implementations should ensure that the
     * leftMapper is only used for error values and rightMapper is only used for error values
     *
     * @param leftMapper  function that will be used to convert the error value
     * @param rightMapper function that will be used to convert the success value
     * @param <U>         the type of return value for both leftMapper and rightMapper
     * @return a representation of the embedded value provided by the application of leftMapper or rightMapper
     */
    <U> U get(Function<L, U> leftMapper, Function<R, U> rightMapper);

    /**
     * Factory method for clients to create new instances of {@link Right}
     *
     * @param rightValue any value that needs to be embedded in Right
     * @param <L>        the type that represents a failure for possible downstream computations
     * @param <R>        the type of rightValue
     * @return an instance of Right with rightValue embedded in it
     */
    static <L, R> Right<L, R> right(R rightValue) {
        return new Right<>(rightValue);
    }

    /**
     * Factory method for clients to create new instances of {@link Left}
     *
     * @param leftValue the error value that will be available to any client trying to use this instance
     * @param <L>       the type of leftValue
     * @param <R>       the type that represents a success for possible downstream computations
     * @return an instance of Left with leftValue embedded in it
     */
    static <L, R> Left<L, R> left(L leftValue) {
        return new Left<>(leftValue);
    }


}
