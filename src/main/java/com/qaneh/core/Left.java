package com.qaneh.core;

import java.util.Objects;
import java.util.function.BiFunction;
import java.util.function.Function;

/**
 * Represents the failure scenario of a computation.
 *
 * @param <L> the type of value that represents an error
 * @param <R> the type of value that represents a success
 * @see Right
 */
public final class Left<L, R> implements Either<L, R> {

    private final L leftValue;

    Left(L leftValue) {
        this.leftValue = leftValue;
    }

    /**
     * Since Left represents a failure, the mapper function is ignored
     *
     * @param mapper a simple function that will map over the data in the Functor instance
     * @param <U>    the return type of mapper
     * @return a new instance of Left that is identical to the current instance
     */
    @Override
    public <U> Either<L, U> map(Function<? super R, ? extends U> mapper) {
        return new Left<>(leftValue);
    }

    /**
     * Since Left represents a failure, functionEither is ignored
     *
     * @param functionEither and Either type that represents a function that may or may not be present
     * @param <U>            return type of the function embedded in functionEither
     * @return a new instance of Left that is identical to the current instance
     */
    @Override
    public <U> Either<L, U> apply(Either<L, ? extends Function<? super R, ? extends U>> functionEither) {
        return new Left<>(leftValue);
    }

    /**
     * Ignores the biMapper and second and returns an instance identical the current instance
     *
     * @param biMapper     the function that needs to be lifted over the current and second Eithers
     * @param secondEither the {@link Either} instance that has (potentially) the second argument for biMapper
     * @return an instance of Either with the return value of biMapper after it is applied to the value of current instance and second
     */
    @Override
    public <U, V> Either<L, V> apply(BiFunction<? super R, ? super U, ? extends V> biMapper, Either<L, U> secondEither) {
        return new Left<>(leftValue);
    }

    /**
     * Ignores the mapper function and returns a new instance of Left identical to the current instance
     */
    @Override
    public <U> Either<L, U> flatMap(Function<? super R, ? extends Either<L, U>> mapper) {
        return new Left<>(leftValue);
    }

    /**
     * Always uses the leftMapper
     *
     * @param leftMapper  function that will be used to convert the error value
     * @param rightMapper function that will be used to convert the success value
     * @return a representation of the embedded value provided by the application of leftMapper
     */
    @Override
    public <U> U get(Function<L, U> leftMapper, Function<R, U> rightMapper) {
        return leftMapper.apply(leftValue);
    }

    @Override
    public String toString() {
        return String.format("Left{leftValue=%s}", leftValue);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Left)) return false;
        Left<?, ?> left = (Left<?, ?>) o;
        return Objects.equals(leftValue, left.leftValue);
    }

    @Override
    public int hashCode() {
        return Objects.hash(leftValue);
    }
}


