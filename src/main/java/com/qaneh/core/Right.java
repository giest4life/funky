package com.qaneh.core;

import java.util.Objects;
import java.util.function.BiFunction;
import java.util.function.Function;

import static com.qaneh.utils.FunctionHelpers.partial;

/**
 * This implementation of {@link Either} represents success values
 *
 * @param <L> the type of value that represents an error
 * @param <R> the type of value that represents a success
 * @see Left
 */
public final class Right<L, R> implements Either<L, R> {

    private final R rightValue;

    Right(R rightValue) {
        this.rightValue = rightValue;
    }

    /**
     * Since {@link Right} represents success, the mapper is applied to the underlying value.
     *
     * @param mapper a simple function that will map over the data in the Functor instance
     * @param <U>    the return type of the result of mapper
     * @return the result of mapper wrapped inside of a new instance of Right
     */
    @Override
    public <U> Right<L, U> map(Function<? super R, ? extends U> mapper) {
        return new Right<>(mapper.apply(rightValue));
    }

    /**
     * Calls map of functionEither and relies on the implementation of the map to enforce the interface's contract
     *
     * @param functionEither and Either type that represents a function that may or may not be present
     * @param <U>            return type of Right after functionEither is called
     * @return the result of applying rightValue to the function embedded in functionEither
     */

    @Override
    public <U> Either<L, U> apply(Either<L, ? extends Function<? super R, ? extends U>> functionEither) {
        return functionEither.map(f -> f.apply(rightValue));
    }

    /**
     * Applies the biMapper to the values inside the current instance and second. This is a convenience method and the same functionality can be achieved with {@link #apply(Either)}
     *
     * @param biMapper     the function that needs to be lifted over the current and second Eithers
     * @param secondEither the {@link Either} instance that has (potentially) the second argument for biMapper
     * @return an instance of Either with the return value of biMapper after it is applied to the value of current instance and second
     */
    @Override
    public <U, V> Either<L, V> apply(BiFunction<? super R, ? super U, ? extends V> biMapper, Either<L, U> secondEither) {
        return secondEither.apply(new Right<>(partial(biMapper, rightValue)));
    }

    /**
     * Applies the mapper function to value embedded in the current instance and obeys the 3 Monad laws
     *
     * @param mapper a function that takes any value of type {@code R} but returns a value of type {@code Either<L,U>}
     * @return the result of applying mapper over the value inside of the instance
     */
    @Override
    public <U> Either<L, U> flatMap(Function<? super R, ? extends Either<L, U>> mapper) {
        return mapper.apply(rightValue);
    }

    /**
     * Always uses the rightMapper
     *
     * @param leftMapper  function that will be used to convert the error value
     * @param rightMapper function that will be used to convert the success value
     * @return a representation of the embedded value provided by the application of rightMapper
     */
    @Override
    public <U> U get(Function<L, U> leftMapper, Function<R, U> rightMapper) {
        return rightMapper.apply(rightValue);
    }

    @Override
    public String toString() {
        return String.format("Right{rightValue=%s}", rightValue);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Right)) return false;
        Right<?, ?> right = (Right<?, ?>) o;
        return Objects.equals(rightValue, right.rightValue);
    }

    @Override
    public int hashCode() {
        return Objects.hash(rightValue);
    }
}
