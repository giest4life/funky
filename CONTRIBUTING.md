# How to Contribute

Feel free to contribute in any capacity that you like. Just keep the following in consideration:

## Workflow

* Associate a merge request with an issue
* Do not expect development branches to be long-lived. The source branch should be deleted following the merge
* Snapshot versions are not deployed
* Version updates to `pom.xml` are prepared using `release.sh` and continuously deployed to staging on push

## Coding Standards

Please follow Java [best practices](https://www.oreilly.com/library/view/effective-java-3rd/9780134686097/ "Effective Java") for naming and coding. The next section mentions some speccific standards that I will try to enforce. Feel free to create issues for existing code that does not meet the standards.

### Design

* Prefer immutability
* Classes should be final, unless extensibility designed for extensibility
* Unless explicitly needed, methods and variables should be non-public
* Avoid throwing checked exceptions

### Testing

* All public methods should have unit tests. Simple getters and setters can be skipped
* Avoid using multiple asserts in an `@Test` method

### Documentation

* Every public interface and its methods should have appropriate Javadocs, including generic parameters and return types
* All public classes and their methods should be accompanied by Javadocs
* Extendable classes extended should explicitly document their invariants
* All constructors and methods should have appropriate Javadocs, except for those with private modifiers
